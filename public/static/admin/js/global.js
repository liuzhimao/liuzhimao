layui.use(["jquery","form","layer","element"],function () {
        var $=layui.jquery,
            element=layui.element;

    });
//提交
function ajax(url,data,type='post',jump=false) {
    $.ajax({
        type:type,
        dataType: 'json',
        url:url,
        data:data,
        beforeSend:function () {

        },
        success:function (res) {
            console.log(res)
            if(res.code===1){
                layer.msg(res.message,{icon:6,time:2000},function () {
                    if(jump==false){
                        window.location.reload();
                    }else{
                        window.location.href=jump
                    }
                });
            }else{
                layer.msg(res.message,{icon:5,time:2000},function () {
                });
            }
        },
        error:function (res) {

        },
        complete:function () {

        }
    })
};
//时间戳转化
function Format(datetime,fmt) {
    if (parseInt(datetime)==datetime) {
        if (datetime.length==10) {
            datetime=parseInt(datetime)*1000;
        } else if(datetime.length==13) {
            datetime=parseInt(datetime);
        }
    }
    datetime=new Date(datetime);
    var o = {
        "M+" : datetime.getMonth()+1,                 //月份
        "d+" : datetime.getDate(),                    //日
        "h+" : datetime.getHours(),                   //小时
        "m+" : datetime.getMinutes(),                 //分
        "s+" : datetime.getSeconds(),                 //秒
        "q+" : Math.floor((datetime.getMonth()+3)/3), //季度
        "S"  : datetime.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
};
function popUpload(obj) {
    var url=$(obj).data("url");
    var id=$(obj).attr("id");
    var $input=$(obj).next("input[type=hidden]");
    layer.open({
        type: 2,
        title: "图片上传",
        closeBtn: 1, //不显示关闭按钮
        btn: ['确定', '取消'] ,
        shade: [0],
        area: ['640px', '460px'],
        anim: 2,
        content:url+"?id="+id,
        yes: function (index, layero) {
            var childbody = layer.getChildFrame('body', index);
            console.log(childbody);
            var _input= childbody.find("ul.filelist input[type=hidden]");
            if(_input.length){
                $input.val(_input.eq(0).val());
                console.log(_input.eq(0).data("img"));
                $(obj).siblings("img").attr("src",_input.eq(0).data("img")).css("display","inline-block");

                /*_input.each(function () {
                    var val=$(this).val();
                    console.log(val)
                })*/
            }
           layer.close(index)
        }
    });
}