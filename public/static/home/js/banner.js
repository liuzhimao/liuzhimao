$(function(){
    var banner_ratio=0.416;
    var  zIndex=2;
    var relHeight=$(window).height();
    var winWidth=$(window).width();
    var banner_length=$("#six_ban li").length;
    var banner_ul=$("#six_ban ul");
    var count=0;
    var  six_timer=null;
    //添加dom
    for(var i=0; i<banner_length; i++){
        $(".six_bandot").append("<span><em></em></span>");
    }
    banner_ul.find("li").eq(0).css("zIndex", zIndex);
    $(".six_bandot span:eq("+0+") em").stop().animate({"width":"100%"},9000);
    $(window).resize(resizeBanner);
    $(window).resize();
    $("#six_ban").stop().animate({"opacity":1});
    function resizeBanner(){
        var changeHeight=$(window).height()
        var changeWidth=$(window).width();
        $("#six_ban").height(changeHeight);
        if(changeHeight/changeWidth<banner_ratio){
            $("#six_ban ul li .intro").css({"width":changeWidth,"height":changeWidth*banner_ratio});
        }else if(relHeight/changeWidth>banner_ratio){
            $("#six_ban ul li .intro").css({"width":changeHeight/banner_ratio,"height":changeHeight});
        }
    }
    function moved(index){
         zIndex++;
         if(index>=banner_length){
             index=0;
             count=0;
         }else{
             index++;
             count=index;
         }
         banner_ul.find("li").eq(index).css("zIndex",zIndex);
         banner_ul.find("li").eq(index).siblings().removeClass("on");
         banner_ul.find("li").eq(index).css({"left":banner_ul.width()});
         banner_ul.find("li").eq(index).stop().animate({"left":0},800);
         $(".six_bandot span em").stop().css("width","0%");
        $(".six_bandot span:eq("+index+") em").stop().animate({"width":"100%"},4000);
    }
    $(".six_bandot span").click(function(){
        clearInterval(six_timer);
        var nIndex=$(".six_bandot span").index(this);
        moved(nIndex);
        six_timer=setInterval(function(){
            moved(nIndex);
        },4000)
    })
    six_timer=setInterval(function(){
        moved(count);
    },4000)
/*end*/
   /* $("#ban_video").each(function(){
        var relHeight=$(window).height();
        var winWidth=$(window).width();
        var _this = $(this);
        var _this_data = _this.data("video");
        console.log(_this_data);
        _this.attr({
            src: _this_data
        })
        // _this.attr({
        //     height: relHeight,
        //     width: winWidth
        // })

    })
    var zhulu_count=0;
    var zhulu_inner_count=-1;
    var zhulu_timer=null;
    var zhulu_zindex=2;
    var relHeight=$(window).height()
    var winWidth=$(window).width();
    var zhulu_banner_ratio=0.416;
    var zhulu_banner_height=$(window).width()*zhulu_banner_ratio;
    var zhulu_banner_size=$("#six_ban li").length;
    for(var i=0; i<zhulu_banner_size; i++){
        $(".zhulu_bandot").append("<span><em></em></span>");
    }
    var zhulu_banner_ul=$("#six_ban ul");
   // var videos = $("#six_ban ul").find("li").eq(1).siblings().find("video").get(0);
    zhulu_banner_ul.find("li").eq(0).css("zIndex",zhulu_zindex);
    $(".zhulu_bandot span:eq("+0+") em").stop().animate({"width":"100%"},9000);
    // $(".zhulu_bandot span:eq("+0+") em").stop().animate({"width":"100%"},4000);
    $(window).resize(resizeBanner);
    $(window).resize();
    $("#six_ban").stop().animate({"opacity":1});
    function resizeBanner(){
        var changeHeight=$(window).height()
        var changeWidth=$(window).width();
        $("#six_ban").height(changeHeight);
        if(changeHeight/changeWidth<zhulu_banner_ratio){
            $("#six_ban ul li .intro").css({"width":changeWidth,"height":changeWidth*zhulu_banner_ratio});
        }else if(relHeight/changeWidth>zhulu_banner_ratio){
            $("#six_ban ul li .intro").css({"width":changeHeight/zhulu_banner_ratio,"height":changeHeight});
        }
    }
    function positionMove(nIndex){
        zhulu_zindex++;
        //zhulu_banner_ul.find("li").eq(1).siblings().find("video").get(0).pause();
        if(zhulu_banner_ul.find("li").eq(nIndex).find("video").length>0){
            zhulu_banner_ul.find("li").eq(nIndex).find("video").get(0).currentTime = 0;
            zhulu_banner_ul.find("li").eq(nIndex).find("video").get(0).play();
        }
        if(zhulu_banner_ul.find("li").eq(nIndex).find(".img_hidden img").length>1){
            if(zhulu_inner_count>=zhulu_banner_ul.find("li").eq(nIndex).find(".img_hidden").find("img").size()-1){
                zhulu_inner_count=0;
            }else{
                zhulu_inner_count++;
            }
            zhulu_banner_ul.find("li").eq(nIndex).find(".all_img").children("a").find("img").attr("src",zhulu_banner_ul.find("li").eq(nIndex).find(".img_hidden").find("img").eq(zhulu_inner_count).attr("src"));
        }
        zhulu_banner_ul.find("li").eq(nIndex).css("zIndex",zhulu_zindex);
        zhulu_banner_ul.find("li").eq(nIndex).siblings().removeClass("on");
        zhulu_banner_ul.find("li").eq(nIndex).css({"left":zhulu_banner_ul.width()});
        zhulu_banner_ul.find("li").eq(nIndex).stop().animate({"left":0},800);
        $(".zhulu_bandot span em").stop().css("width","0%");
        if(nIndex==0){
            $(".zhulu_bandot span:eq(0) em").stop().animate({"width":"100%"},9000);
        }else{
            $(".zhulu_bandot span:eq("+nIndex+") em").stop().animate({"width":"100%"},4000);
        }
        // $(".zhulu_bandot span:eq("+nIndex+") em").stop().animate({"width":"100%"},4000);
    }
    function rightMove(){
        if(zhulu_count>=zhulu_banner_size-1){
            zhulu_count=0;
            positionMove(zhulu_count);
        }else{
            zhulu_count++;
            positionMove(zhulu_count);
        }
    }
    $(".zhulu_bandot span").click(function(){
        clearInterval(zhulu_timer);
        var nIndex=$(".zhulu_bandot span").index(this);
        positionMove(nIndex);
        zhulu_count=nIndex;
        zhulu_timer=setInterval(function(){
            rightMove();
        },4000)
    })
   /!* videos.addEventListener('pause', function () {
        clearInterval(zhulu_timer);
        zhulu_timer=setInterval(function(){
            rightMove();
        },4000)
    }, false);
    videos.addEventListener('play', function () {  
        clearInterval(zhulu_timer);
       zhulu_timer=setInterval(function(){
            rightMove();
        },9000)
    }, false);*!/
    // zhulu_timer=setInterval(function(){
    //     rightMove();
    // },4000)*/
});