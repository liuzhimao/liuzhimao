<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://wangshuwen.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | Home应用基类
// +----------------------------------------------------------------------
namespace app\common\controller;


use app\home\model\Category;
use think\facade\View;

class HomeBase extends Base
{
    protected function initialize()
    {
        parent::initialize();
        $this->initNav();
    }

    public function initNav()
    {
        View::assign('navs',Category::order('sort')->where('type','nav')->select());
    }

}