<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://wangshuwen.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | Admin应用基类
// +----------------------------------------------------------------------
namespace app\common\controller;


use app\admin\middleware\Authenticate;
use app\admin\middleware\Login;

class AdminBase extends Base
{
    protected $middleware = [Login::class,Authenticate::class];

    protected function initialize()
    {
        parent::initialize();
    }

    public function layuiTableJson($count,$data)
    {
        return json(['code'=>0,'count'=>$count,'data'=>$data]);
    }

    public function success($message, $url = '', $data = '')
    {
        return json(['code'=>1,'url'=>$url,'message'=>$message,'data'=>$data]);
    }

    public function error($message, $url = '', $data = '')
    {
        return json(['code'=>0,'url'=>$url,'message'=>$message,'data'=>$data]);
    }



}