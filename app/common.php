<?php
// 应用公共文件

/**
 * 六只猫密码加密方法
 * @param string $password 要加密的原始密码
 * @param string $encrypt  加密字符串
 * @return string
 */
function lzm_password($password, $encrypt = '')
{
    if (empty($encrypt)) {
        $encrypt = config('cms.encrypt');
    }
    return md5(md5($encrypt . $password));
}
