<?php
namespace app\home\controller;

use app\common\controller\HomeBase;
use app\home\model\Article;
use app\Request;

class Articles extends HomeBase
{
    public function index()
    {
        $articles = Article::order('sort')
            ->where('type','article')
            ->where('status',1)
            ->select();

        return view('index',[
            'articles' => $articles
        ]);
    }

    public function show(Request $request)
    {
        $article = Article::find($request->get('id'));

        return view('show',[
            'article' => $article
        ]);
    }

}
