<?php
namespace app\home\controller;

use app\common\controller\HomeBase;
use app\home\model\Article;
use app\home\model\Slide;

class Index extends HomeBase
{
    public function index()
    {
        $slides = Slide::order('sort')->where('category_id',8)->select();
        $articles = Article::order('sort')
            ->where('type','article')
            ->where('status',1)
            ->limit(4)
            ->select();
        return view('index',[
            'slides' => $slides,
            'articles' => $articles
        ]);
    }

}
