<?php
namespace app\home\model;

use think\Model;

class Article extends Model
{
    protected $autoWriteTimestamp = true;

    public function getPreviewThumbImageAttr()
    {
        return 'http://liuzhimao.test/storage/'.$this->thumb_image;
    }

}
