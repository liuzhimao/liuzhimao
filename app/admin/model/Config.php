<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 网站配置模型
// +----------------------------------------------------------------------
namespace app\admin\model;

use \think\Model;

class Config extends Model
{

    protected $autoWriteTimestamp = true;


    public function getArrayValueAttr()
    {
        return json_decode($this->value,JSON_UNESCAPED_UNICODE);
    }

}
