<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 分类模型
// +----------------------------------------------------------------------
namespace app\admin\model;


use \think\Model;

class Category extends Model
{
    protected $autoWriteTimestamp = true;

    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id','id');
    }

    public static  function onBeforeWrite(Category $category)
    {
        if (empty($category->parent_id)) {
            $category->level = 0;
            $category->path  = '-';
        } else {
            $category->level = $category->parent->level + 1;
            $category->path  = $category->parent->path.$category->parent_id.'-';
        }
    }

    public function getPreviewImageAttr()
    {
        return 'http://liuzhimao.test/storage/'.$this->image;
    }

}
