<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 幻灯片模型
// +----------------------------------------------------------------------
namespace app\admin\model;


use \think\Model;

class Slide extends Model
{
    protected $autoWriteTimestamp = true;

    public function getPreviewImageAttr()
    {
        return 'http://liuzhimao.test/storage/'.$this->image;
    }

}
