<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 上传管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Admin;
use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;
use app\common\controller\AdminBase;
use think\facade\Db;
use think\Request;

class Uploads extends AdminBase
{
    public function image(Request $request)
    {
        if($request->isPost()){
            $file = request()->file('file');
            $image = \think\facade\Filesystem::disk('public')->putFile( 'images', $file);
            $preview_image = 'http://liuzhimao.test/storage/'.$image;
            return $this->success('上传成功','',['image'=>$image,'preview_image'=>$preview_image]);
        }
        return view();
    }

}
