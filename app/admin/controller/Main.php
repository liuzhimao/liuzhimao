<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 后台框架
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\controller\AdminBase;

class Main extends AdminBase
{

    public function index()
    {

        return view('index',[
           'SUBMENU_CONFIG' =>  json_encode(app('app\admin\model\menu')->getMenuList()),
        ]);
    }

}
