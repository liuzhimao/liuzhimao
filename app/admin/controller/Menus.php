<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 菜单管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Menu;
use app\common\controller\AdminBase;
use think\Request;

class Menus extends AdminBase
{
    public function index(Request $request)
    {
        if($request->isAjax()){
            $bulider = Menu::order('sort');

            return json(['code'=>0,'message'=>'获取数据成功','count'=>$bulider->count(),'data'=>$bulider->select()->toArray()]);
        }
        return view();
    }

    public function apiListOrder(Request $request)
    {
        if($request->isAjax()){
            $menu = Menu::find($request->post('id/d'));
            $menu->sort = $request->post('value/d');
            $menu->save();
            return json(['code'=>1,'message'=>'操作成功']);
        }
    }

    public function apiSetStatus(Request $request)
    {
        if($request->isAjax()){
            $menu = Menu::find($request->get('id/d'));
            $menu->status = $request->get('status/d');
            $menu->save();
            return json(['code'=>1,'message'=>'操作成功']);
        }
    }

    public function apiGetMenu(Request $request, Menu $menu)
    {
        if($request->isAjax()){
            $first_menu = [['id'=>0,'title'=>'作为一级菜单','parent_id'=>0,'is_directory'=>0]];
            $menues = $menu
                ->field('id,title,parent_id,is_directory')
                ->order('sort')
                ->where('parent_id',$request->get('parent_id/d'))
                ->select()->toArray();

            return $this->success('数据获取成功','',$request->get('parent_id/d')===0?array_merge($first_menu,$menues):$menues);
        }
    }

    public function create(Request $request)
    {
        $id = $request->get('id/d');
        $chooseData = [0];
        if(isset($id) and !empty($id) and $menu = Menu::find($id)){
            if($menu->path === '-'){
                $chooseData = [$menu->id];
            }else{
                $chooseData = explode('-',trim($menu->path,'-'));
                array_push($chooseData,$menu->id);
            }
        }
        return view('create',[
            'chooseData' => json_encode($chooseData),
            'parent_id' => $id
        ]);
    }

    public function save(Request $request, Menu $menu)
    {
        $menu->save($request->only(['parent_id','title','icon','app','controller','action','parameter','status','is_directory','is_menu']));
        return $this->success('操作成功','index');
    }

    public function edit(Request $request)
    {
        $menu = Menu::find($request->get('id/d'));
        return view('edit',[
            'menu'=>$menu,
            'chooseData' => ($menu->path=='-')?json_encode([0]):json_encode(explode('-',trim($menu->path,'-')))
        ]);
    }

    public function update(Request $request)
    {
        $menu = Menu::find($request->post('id/d'));
        $menu->save($request->only(['parent_id','title','icon','app','controller','action','parameter','status','is_directory','is_menu']));
        return $this->success('操作成功','index');
    }

    public function delete(Request $request)
    {
        $menu = Menu::find($request->get('id/d'));
        $menu->delete();

        return $this->success('删除成功');
    }

}
