<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 后台登录相关
// +----------------------------------------------------------------------
namespace app\admin\controller;


use app\admin\model\Admin;
use think\Request;

class Login
{

    /**
     * 后台登陆页面
     * @return \think\response\View
     */
    public function create()
    {
        return view();
    }

    public function save(Request $request, Admin $admin)
    {
        $admin = $admin->where('username',$request->post('username'))->find();

        if(empty($admin) or !isset($admin)){
            return json(['code'=>0,'message'=>'用户不存在']);
        }

        if($admin->status !== 1){
            return json(['code'=>0,'message'=>'用户被锁定']);
        }

        if($admin->password !== lzm_password($request->post('password'))){
            return json(['code'=>0,'message'=>'密码不正确']);
        }

        session('admin',$admin);
        $admin->last_login_time = time();
        $admin->last_login_ip = $request->ip();
        $admin->save();
        return json(['code'=>1,'message'=>'登录成功']);

    }

    public function keepLogin()
    {
        return json(['code'=>1,'message'=>'维持登录']);
    }

    public function delete()
    {
        session('admin',null);
        return json(['code'=>1,'message'=>'登出成功']);
    }

}
