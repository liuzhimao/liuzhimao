<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 后台角色管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;
use app\admin\model\AuthRule;
use app\admin\model\Menu;
use app\common\controller\AdminBase;
use think\facade\Db;
use think\Request;

class Roles extends AdminBase
{
    public function index(Request $request)
    {
        if($request->isAjax()){
            $bulider = AuthGroup::order('id','desc');

            return json(['code'=>0,'message'=>'获取数据成功','count'=>$bulider->count(),'data'=>$bulider->select()->toArray()]);
        }
        return view();
    }

    public function create()
    {
        return view();
    }

    public function apiGetMenus(Request $request, Menu $menu)
    {
        if($request->isAjax()) {
            $menus = $menu->field('id,title,parent_id')->order('sort')->select();

            $checkedId = [];
            $auth_group_id = $request->get('auth_group_id');
            if(isset($auth_group_id) and !empty($auth_group_id)){
                $checkedId = array_values(AuthRule::where('auth_group_id',$auth_group_id)->column('rule'));
                foreach ($checkedId as $k=>$v){
                    $checkedId[$k] = (int) $v;
                }
            }

            return $this->success('获取数据成功', '', ['list' => $menus, 'checkedId' => $checkedId]);
        }
    }

    public function save (Request $request, AuthGroup $authGroup, AuthRule $authRule)
    {
        Db::transaction(function () use ($request,$authGroup,$authRule) {
            $authGroup->save($request->only(['title','description','status']));
            $data = [];
            foreach ($request->post('rules') as $rule){
                array_push($data,['auth_group_id'=>$authGroup->id,'rule'=>$rule]);
            }
            $authRule->saveAll($data);
        });
        return $this->success('角色添加成功','index');
    }

    public function delete(Request $request)
    {
        if(AuthGroupAccess::where('auth_group_id',$request->get('id/d'))->find()){
            $this->error('该角色下有相应管理员，不可删除');
        }
        Db::transaction(function () use ($request) {
            AuthGroup::destroy($request->get('id/d'));
            AuthRule::where('auth_group_id',$request->get('id/d'))->delete();
            AuthGroupAccess::where('auth_group_id',$request->get('id/d'))->delete();
        });

        return $this->success('删除成功');
    }

    public function edit(Request $request, AuthGroup $authGroup)
    {
        $authGroup = $authGroup->find($request->get('id/d'));

        return view('edit',[
            'authGroup' => $authGroup
        ]);
    }

    public function update(Request $request, AuthGroup $authGroup, AuthRule $authRule)
    {
        Db::transaction(function () use ($request,$authGroup,$authRule) {
            $authGroup = $authGroup->find($request->post('id/d'));
            $authGroup->save($request->only(['title','description','status']));

            $authRule->where('auth_group_id',$request->post('id/d'))->delete();

            $data = [];
            foreach ($request->post('rules') as $rule){
                array_push($data,['auth_group_id'=>$authGroup->id,'rule'=>$rule]);
            }
            $authRule->saveAll($data);
        });
        return $this->success('角色修改成功','index');
    }

}
