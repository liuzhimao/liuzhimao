<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 幻灯片管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Category;
use app\admin\model\Slide;
use app\common\controller\AdminBase;
use app\Request;

class Slides extends AdminBase
{
    public function index(Request $request)
    {
        if($request->isAjax()){
            $bulider = Slide::order(['sort','id'=>'desc'])
                ->page($request->get('page/d'),$request->get('limit/d'));

            return $this->layuiTableJson($bulider->count(),$bulider->select());
        }
        return view();
    }

    public function apiGetCategory(Request $request, Category $category)
    {
        if($request->isAjax()){
            $categories = $category
                ->field('id,title,parent_id,is_directory')
                ->order('sort')
                ->where('parent_id',$request->get('parent_id/d'))
                ->where('type','slide')
                ->select()->toArray();

            return $this->success('数据获取成功','',$categories);
        }
    }

    public function create()
    {
        return view();
    }

    public function save(Request $request, Slide $slide)
    {
        $slide->data($request->only(['category_id','title','description','image','url','status','sort']));
        $slide->type = 'slide';
        $slide->save();

        return $this->success('添加成功','index');
    }

    public function edit(Request $request)
    {
        $slide = Slide::find($request->get('id/d'));
        $category = Category::find($slide->category_id);

        return view('edit',[
            'slide' => $slide,
            'chooseData' => json_encode(explode('-',trim($category->path.$category->id,'-')))
        ]);
    }

    public function update(Request $request)
    {
        $slide = Slide::find($request->post('id/d'));
        $slide->save($request->only(['category_id','title','description','image','url','status','sort']));
        return $this->success('保存成功','index');
    }

}
