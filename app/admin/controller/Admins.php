<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 后台管理员管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Admin;
use app\admin\model\AuthGroup;
use app\admin\model\AuthGroupAccess;
use app\common\controller\AdminBase;
use think\facade\Db;
use think\Request;

class Admins extends AdminBase
{
    public function index(Request $request)
    {
        if($request->isAjax()){
            $bulider = Admin::order('id')->page($request->get('page'),$request->get('limit'));
            return $this->layuiTableJson($bulider->count(),$bulider->select());
        }
        return view();
    }

    public function create()
    {
        return view('create',['authGroups' => AuthGroup::where('status',1)->select()]);
    }

    public function save(Request $request, AuthGroupAccess $authGroupAccess)
    {
        Db::transaction(function () use ($request,$authGroupAccess) {
            $admin = new Admin($request->only(['username','nickname','status']));
            $admin->password = lzm_password($request->post('password'));
            $admin->save();

            $data = [];
            foreach ($request->post('role') as $auth_group_id){
                array_push($data,['auth_group_id'=>$auth_group_id,'admin_id'=>$admin->id]);
            }
            $authGroupAccess->saveAll($data);
        });
        return $this->success('管理员添加成功','index');
    }

    public function edit(Request $request)
    {

        $admin = Admin::find($request->get('id/d'));
        $authGroups = AuthGroup::where('status',1)->select();

        foreach ($authGroups as $key => $authGroup){
            $authGroups[$key]['checked'] = !empty(AuthGroupAccess::where('admin_id',$admin->id)
                ->where('auth_group_id',$authGroup['id'])->find())?true:false;
        }

        return view('edit',['authGroups' => $authGroups, 'admin' => $admin]);
    }

    public function update(Request $request, AuthGroupAccess $authGroupAccess)
    {
        Db::transaction(function () use ($request,$authGroupAccess) {
            $admin = Admin::find($request->post('id/d'));
            $admin->data($request->only(['username','nickname','status']));
            $admin->save();

            $authGroupAccess->where('admin_id',$request->post('id/d'))->delete();
            $data = [];
            foreach ($request->post('role') as $auth_group_id){
                array_push($data,['auth_group_id'=>$auth_group_id,'admin_id'=>$request->post('id/d')]);
            }
            $authGroupAccess->saveAll($data);
        });
        return $this->success('数据保存成功','index');
    }

    public function password(Request $request)
    {
        if($request->isAjax()){
            if(empty($request->post('id/d'))){
                return $this->error('缺少必要参数ID');
            }
            if(Admin::where('id',1)->value('password') !== lzm_password($request->post('admin_password'))){
                return $this->error('授权失败');
            }
            $admin = Admin::find($request->post('id/d'));
            $admin->password = lzm_password($request->post('password'));
            $admin->save();
            return $this->success('密码修改成功','index');
        }
        return view('password',['id' => $request->get('id/d')]);
    }

    public function delete(Request $request)
    {
        Db::transaction(function () use ($request) {
            Admin::where('id',$request->get('id/d'))->delete();
            AuthGroupAccess::where('admin_id',$request->get('id/d'))->delete();
        });
        return $this->success('删除成功','index');
    }

}
