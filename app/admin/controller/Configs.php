<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 系统配置
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Config;
use app\common\controller\AdminBase;
use app\Request;
use think\Collection;

class Configs extends AdminBase
{
    public function index(Config $config)
    {
        $config_group = $config->where('key','config_group')->find();

        $data = new Collection();
        foreach ($config_group->arrayValue as $key=>$value){
            $data->push($config->where('group',$key)->select());
        }

        return view('index',[
            'config_group' => $config_group,
            'data' => $data
        ]);
    }

    public function update(Request $request, Config $config)
    {
        foreach ($request->post() as $key=>$value){
            $type = $config->where('key',$key)->value('type');
            switch ($type){
                case 'string':
                    $config->where('key',$key)->save(['value'=>$value]);
                    break;
                case 'array':
                    $data = [];
                    foreach ($value['key'] as $k=>$v){
                        $data[$v] = $value['value'][$k];
                    }
                    $config->where('key',$key)->save(['value'=>json_encode($data,JSON_UNESCAPED_UNICODE)]);
                    break;
                default:
                    break;
            }
        }

        return $this->success('保存成功');

    }


}
