<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://erdangjiade.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: xiaojie <997031758@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 登录认证中间件
// +----------------------------------------------------------------------
namespace app\admin\middleware;

use app\admin\model\Admin;

class Login
{
    public function handle($request, \Closure $next)
    {
        if(empty(Admin::user())){
            return redirect('/admin/login/create');
        }

        return $next($request);
    }
}
